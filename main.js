Vue.use(VueNumeric.default);

function calculateMonthlyCost(loanAmount, repaymentYears, interest) {
    var months = repaymentYears * 12;

    return Math.round(loanAmount * (interest / 100) / 12 / (1 - Math.pow(1 + (interest / 100) / 12, (months * -1))));
}

var app = new Vue({
    'el': '#app',
    data() {
        return {
            content: null,
            currentAmount: 250000,
            currentLength: 14,
            currentCost: null

        };
    },
    created() {
        this.content = this.getContent();
        this.calculateCost();
    },
    methods: {
        /**
         * Simulates an API call to a CMS.
         */
        getContent() {
            return {
                "monthlyCostLabel": "Månadskostnad",
                "monthlyCostSuffix": "kr",
                "loanAmountLabel": "Lånebelopp",
                "loanAmountSuffix": "kr",
                "repaymentYearsLabel": "Återbetalningstid",
                "repaymentYearsSuffix": "år",
                "ctaLabel": "Ansök nu",
                "interest": 5.77
            };
        },
        incrementAmount() {
            if (this.currentAmount < 600000) {
                this.currentAmount += 5000;
                this.calculateCost();
            }
        },
        decrementAmount() {
            if (this.currentAmount > 5000) {
                this.currentAmount -= 5000;
                this.calculateCost();
            }
        },
        incrementYears() {       
            if (this.currentLength < 15 ) {
                this.currentLength += 1;
                this.calculateCost();
            }
        },
        decrementYears() {
            if (this.currentLength > 1 ) {
                this.currentLength -= 1;
                this.calculateCost();
            }
        },
        calculateCost() {
            this.currentCost = calculateMonthlyCost(this.currentAmount, this.currentLength, this.content.interest);
        }
    }
});
